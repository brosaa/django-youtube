"""Data to keep between HTTP requests (app state)

* selected: list of selected videos
* selectable: list of selectable videos
"""

selected = []
selectable = []

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>Django YouTube (version 1)</h1>
    <h2>Selected</h2>
      <ul>
      {selected}
      </ul>
    <h2>Selectable</h2>
      <ul>
      {selectable}
      </ul>
  </body>
</html>
"""

VIDEO = """
      <li>
        <form action='/' method='post'>
          <a href='{id}'>{title}</a>
          {id}
          <input type='hidden' name='id' value='{id}'>
          <input type='hidden' name='csrfmiddlewaretoken' value='{token}'>
          <input type='hidden' name='{name}' value='True'> 
          <input type='submit' value='{action}'>
        </form>
      </li>
"""

INFO = """
<!DOCTYPE html>
<html lang="en">
    <body>
        <h2>Titulo del video: <a href='{video[link]}'>{video[title]}</a></h2>
        <h2><a href='{video[link]}'><img src={video[image]}></a></h2>
        <h3>Nombre del canal: <a href='{video[urlcanal]}'>{video[canalname]}</a></h2>
        <h4>Fecha de publicación: {video[date]}</h2>
        <h4>Descripción: {video[description]}</h2>
    </body>
</html>
"""

NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
    <body>
        <h3>El identificador {id} no se corresponde con ningún video de la lista de seleccionados</h3>
    </body>
</html>
"""
